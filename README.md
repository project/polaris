Remember to add the following keys to your site in order for the module to work:

* POLARIS_API_ACCESS_ID
* POLARIS_API_ACCESS_KEY
* POLARIS_API_HOST
* POLARIS_API_STAFF_ID
* POLARIS_API_STAFF_DOMAIN
* POLARIS_API_STAFF_USERNAME
* POLARIS_API_STAFF_PASSWORD
* POLARIS_API_WORKSTATION_ID
* POLARIS_API_DEFAULT_PATRON_BRANCH_ID

TODO:
- Polaris core client in src/RCPL should be added as a new project on packagist
- Add composer.json file for dependency on once in packagist
- Fix call to ExternalAuth::loginRegister during the validate function
- Move direct call to 'authmap' db update to the event subscriber
