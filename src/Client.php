<?php

namespace Drupal\polaris;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\externalauth\Authmap;
use Drupal\key\KeyRepository;
use RCPL\Polaris\Client as PolarisClient;

/**
 * Class Client.
 */
class Client extends PolarisClient {

  /**
   * Drupal\key\KeyRepository definition.
   *
   * @var \Drupal\key\KeyRepository
   */
  protected $keyRepository;

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * @var \Drupal\externalauth\Authmap
   */
  protected $authmap;

  /**
   * Constructs a new Client object.
   */
  public function __construct(KeyRepository $key_repository, EntityTypeManagerInterface $entity_type_manager, Authmap $authmap) {
    $this->keyRepository = $key_repository;
    $this->entityTypeManager = $entity_type_manager;
    $this->authmap = $authmap;

    $params = [
      'ACCESS_ID'                => $this->keyRepository->getKey('polaris_api_access_id')->getKeyValue(),
      'ACCESS_KEY'               => $this->keyRepository->getKey('polaris_api_access_key')->getKeyValue(),
      'HOST'                     => $this->keyRepository->getKey('polaris_api_host')->getKeyValue(),
      'STAFF_DOMAIN'             => $this->keyRepository->getKey('polaris_api_staff_domain')->getKeyValue(),
      'STAFF_ID'                 => $this->keyRepository->getKey('polaris_api_staff_id')->getKeyValue(),
      'STAFF_USERNAME'           => $this->keyRepository->getKey('polaris_api_staff_username')->getKeyValue(),
      'STAFF_PASSWORD'           => $this->keyRepository->getKey('polaris_api_staff_password')->getKeyValue(),
      'WORKSTATION_ID'           => $this->keyRepository->getKey('polaris_api_workstation_id')->getKeyValue(),
      'DEFAULT_PATRON_BRANCH_ID' => $this->keyRepository->getKey('polaris_api_default_patron_branch_id')->getKeyValue(),
    ];
    parent::__construct($params);
  }

  /**
   * {@inheritdoc}
   */
  protected function getControllerClass($name) {
    if (class_exists('\Drupal\polaris\\' . $name)) {
      return '\Drupal\polaris\\' . $name;
    }
    return parent::getControllerClass($name);
  }

  /**
   * @return EntityTypeManagerInterface
   */
  public function entityTypeManager() {
    return $this->entityTypeManager;
  }

  /**
   * @return Authmap
   */
  public function authmap() {
    return $this->authmap;
  }

}
