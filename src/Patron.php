<?php

namespace Drupal\polaris;

use Drupal\user\UserInterface;
use RCPL\Polaris\Controller\Patron as PolarisPatron;

class Patron extends PolarisPatron {

  /**
   * Load a Patron object by Drupal user ID.
   *
   * @param $uid
   * @return bool|static
   */
  public function getByUid($uid) {
    $user = $this->client->entityTypeManager->getStorage('user')->load($uid);
    return $user ? $this->getByUser($user) : FALSE;
  }

  /**
   * Load a Patron object by a Drupal user object.
   *
   * @param \Drupal\user\UserInterface $user
   *   The User object.
   *
   * @return bool|static
   */
  public function getByUser(UserInterface $user) {
    $authdata = $this->client->authmap->getAuthData($user->id(), 'polaris');
    if (empty($authdata['data'])) {
      return FALSE;
    }
    $authdata = unserialize($authdata['data']);
    return !empty($authdata->Barcode) ? $this->get($authdata->Barcode) : FALSE;
  }

    /**
     * Load a User object from a Polaris barcode.
     *
     * @param $barcode
     *
     * @return bool|\Drupal\user\Entity\User
     */
  public function getUserByBarcode($barcode) {
    if ($uid = $this->client->authmap->getUid($barcode, 'polaris')) {
      return $this->client->entityTypeManager()->getStorage('user')->load($uid);
    }
    return FALSE;
  }

  public function searchBasic(array $values = []) {
    $query = [];
    if (!empty($values['first_name'])) {
      $query['PATNF'] = $values['first_name'] . '*';
    }
    if (!empty($values['last_name'])) {
      $query['PATNL'] = $values['last_name'] . '*';
    }
    if (!empty($values['email'])) {
      $query['EM'] = $values['email'];
    }
    $results = $this->searchAnd($query);
    return $this->processSearchBasic($results);
  }

  protected function processSearchBasic($results) {
    if (empty($results->PatronSearchRows)) {
      return [];
    }
    $return = [];
    $results = $results->PatronSearchRows;
    foreach ($results as $result) {
      $patron = $this->get($result->Barcode);
      $return[$result->Barcode] = [
        'name' => $this->formatName($result->PatronFirstLastName),
        'barcode' => $result->Barcode,
        'email' => $patron->data()->EmailAddress,
      ];
    }

    return $return;
  }

  /**
   * Format Polaris returned name to readable.
   */
  public function formatName($name) {
    if (empty($name)) {
      return '';
    }
    $name = array_reverse(explode(',', $name));
    $name = array_map('trim', $name);
    return implode(' ', $name);
  }
}
