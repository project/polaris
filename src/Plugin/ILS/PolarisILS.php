<?php

/**
 * @file
 * Contains \Drupal\polaris\Plugin\ILS\PolarisILS.
 * Defines a plugin of type "ILS" for use with the intercept_ils module which
 * is part of the Intercept project at https://drupal.org/project/intercept
 */

namespace Drupal\polaris\Plugin\ILS;

use Drupal\intercept_ils\ILSBase;
use Drupal\polaris\Client;
// The container factory plugin interface is crucial if passing a service
// using dependency injection.
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides access to the Polaris ILS for use in Intercept.
 *
 * @ILS(
 *   id = "polaris",
 *   name = "Polaris",
 * )
 */
class PolarisILS extends ILSBase implements ContainerFactoryPluginInterface {

  /**
   * Drupal\polaris\Client definition.
   *
   * @var \Drupal\polaris\Client
   */
  protected $client;

  /**
   * Constructs a new PolarisILS object.
   * @param Client $client
   *   polaris.client service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Client $client) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = $client;
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('polaris.client')
    );
  }

  public function getClient() {
    return $this->client;
  }
}
