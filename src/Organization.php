<?php

namespace Drupal\polaris;

use RCPL\Polaris\Controller\Organization as PolarisOrganization;
use Drupal\node\NodeInterface;

class Organization extends PolarisOrganization {

  public function getByNode(NodeInterface $node) {
    if (!$node->hasField('field_polaris_id')) {
      return FALSE;
    }
    $id = $node->field_polaris_id->getString();
    return $id ? $this->client->organization->getById($id) : FALSE;
  }
}
