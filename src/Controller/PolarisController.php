<?php

namespace Drupal\polaris\Controller;

use Drupal\Component\Utility\Crypt;
use Drupal\Component\Utility\Xss;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DateFormatterInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\polaris\Form\PolarisPinResetForm;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

/**
 * Controller routines for polaris routes.
 */
class PolarisController extends ControllerBase {

  /**
   * The date formatter service.
   *
   * @var \Drupal\Core\Datetime\DateFormatterInterface
   */
  protected $dateFormatter;

  /**
   * A logger instance.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a PolarisController object.
   *
   * @param \Drupal\Core\Datetime\DateFormatterInterface $date_formatter
   *   The date formatter service.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   */
  public function __construct(DateFormatterInterface $date_formatter, LoggerInterface $logger) {
    $this->dateFormatter = $date_formatter;
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('date.formatter'),
      $container->get('logger.factory')->get('polaris')
    );
  }

  /**
   * Redirects to the Polaris PIN reset form.
   *
   * In order to never disclose a reset link via a referrer header this
   * controller must always return a redirect response.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $library_card_number
   *   Library card number or Polaris username of user requesting reset.
   * @param int $timestamp
   *   The current timestamp.
   * @param string $hash
   *   Login link hash.
   */
  public function resetPin(Request $request, $library_card_number, $timestamp, $hash) {
    $account = $this->currentUser();
    // When processing the one-time login link, we have to make sure that a user
    // isn't already logged in.
    if ($account->isAuthenticated()) {
      user_logout();
      // We need to begin the redirect process again because logging out will
      // destroy the session.
      return $this->redirect(
        'polaris.reset',
        [
          'library_card_number' => $library_card_number,
          'timestamp' => $timestamp,
          'hash' => $hash,
        ]
      );
    }

    $session = $request->getSession();
    $session->set('pass_reset_hash', $hash);
    $session->set('pass_reset_timeout', $timestamp);
    return $this->redirect(
      'polaris.reset.form',
      ['library_card_number' => $library_card_number]
    );
  }

  /**
   * Returns the user password reset form.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request.
   * @param string $library_card_number
   *   Library card number or Polaris username of user requesting reset.
   *
   * @return array|\Symfony\Component\HttpFoundation\RedirectResponse
   *   The form structure or a redirect response.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   If the pass_reset_timeout or pass_reset_hash are not available in the
   *   session.
   */
  public function getResetPinForm(Request $request, $library_card_number) {
    $session = $request->getSession();
    $timestamp = $session->get('pass_reset_timeout');
    $hash = $session->get('pass_reset_hash');
    // As soon as the session variables are used they are removed to prevent the
    // hash and timestamp from being leaked unexpectedly. This could occur if
    // the user does not click on the log in button on the form.
    $session->remove('pass_reset_timeout');
    $session->remove('pass_reset_hash');
    if (!$hash || !$timestamp) {
      throw new AccessDeniedHttpException();
    }

    // Time out, in seconds, until login URL expires.
    $timeout = $this->config('user.settings')->get('password_reset_timeout');

    $expiration_date = $this->dateFormatter->format($timestamp + $timeout);
    return $this->formBuilder()->getForm(PolarisPinResetForm::class, $library_card_number, $expiration_date, $timestamp, $hash);
  }

  /**
   * Validates user, hash, and timestamp; logs the user in if correct.
   *
   * @param string $library_card_number
   *   Library card number or Polaris username of user requesting reset.
   * @param int $timestamp
   *   The current timestamp.
   * @param string $hash
   *   Login link hash.
   *
   * @return \Symfony\Component\HttpFoundation\RedirectResponse
   *   Returns a redirect to the user edit form if the information is correct.
   *   If the information is incorrect redirects to 'polaris.forgot' route with a
   *   message for the user.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   If $library_card_number is for a blocked user or invalid card number.
   */
  public function resetPinLogin($library_card_number, $timestamp, $hash) {
    // The current user is not logged in, so check the parameters.
    $current = \Drupal::time()->getRequestTime();

    // In the standard User module, the customer should always have a matching account.
    // In the case of the Polaris module, the customer may or may not have an existing Drupal account.
    //if ($user === NULL || !$user->isActive()) {
    $user = user_load_by_name($library_card_number);
    if (empty($user)) {
      // Let's check Polaris to see if this is an existing account there.
      $client = \Drupal::service('polaris.client');
      $patron = $client->patron->get($library_card_number);
      $data = $patron->basicData();
      if (empty($data->PatronID)) {
        $this->messenger()->addStatus('No matching user account was found in the catalog.');
      }
      else {
        $account_data = [
          'name' => $patron->barcode(),
          'mail' => $data->EmailAddress,
          'init' => $data->EmailAddress,
        ];
        // Create a Drupal user automatically and return the new user_id.
        $externalauth = \Drupal::service('externalauth.externalauth');
        $user = $externalauth->register($patron->barcode(), 'polaris', $account_data, $data);
      }
      // We should be able to pull a user account now.
      $user = user_load_by_name($library_card_number);
      $user->setLastLoginTime('0'); // Mark them as never having logged in.
    }
    $email = $user->getEmail();
    // If they do at this point have an account, log them in and allow them to reset the PIN.

    // Time out, in seconds, until login URL expires.
    $timeout = $this->config('user.settings')->get('password_reset_timeout');
    // No time out for first time login.
    if ($user->getLastLoginTime() && $current - $timestamp > $timeout) {
      $this->messenger()->addError($this->t('You have tried to use a one-time login link that has expired. Please request a new one using the form below.'));
      return $this->redirect('polaris.forgot');
    }
    elseif ($user->isAuthenticated() && $timestamp >= $user->getLastLoginTime() && $timestamp <= $current && hash_equals($hash, polaris_pin_rehash($library_card_number, $email, $timestamp))) {
      user_login_finalize($user);
      $this->logger->notice('User %name used one-time login link at time %timestamp.', ['%name' => $user->getDisplayName(), '%timestamp' => $timestamp]);
      $this->messenger()->addStatus($this->t('You have just used your one-time login link. It is no longer necessary to use this link to log in. Please change your PIN.'));
      // Set their last login time to now.
      $user->setLastLoginTime($current);
      // Let the user's PIN be changed without the current PIN
      // check.
      $token = Crypt::randomBytesBase64(55);
      $_SESSION['pass_reset_' . $user->id()] = $token;
      return $this->redirect(
        'polaris.reset.pin'
      );
    }

    $this->messenger()->addError($this->t('You have tried to use a one-time login link that has either been used or is no longer valid. Please request a new one using the form below.'));
    return $this->redirect('polaris.forgot');
  }

  /**
   * Show a message to the customer based on the results of their PIN submission.
   */
  public function forgotPinFormSubmitted() {
    $build = [
      '#markup' => '<h1>Forgot Your PIN?</h1><p><a href="/polaris/forgot-pin" class="button button--arrow-green">Try again</a>',
    ];
    return $build;
  }

}
