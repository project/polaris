<?php

namespace Drupal\polaris\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\externalauth\Event\ExternalAuthEvents;
use Drupal\user\Entity\Role;
use Drupal\user\UserInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class PolarisPatronEventSubscriber.
 */
class PolarisPatronEventSubscriber implements EventSubscriberInterface {

  /**
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManager;

  /**
   * Constructs a new PolarisPatronEventSubscriber object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {

    $events[ExternalAuthEvents::REGISTER] = ['onRegister'];

    $events[ExternalAuthEvents::LOGIN] = ['onLogin'];

    return $events;
  }

  /**
   *
   */
  public function onLogin($event) {
    // Currently not actually triggered with this configuration.
  }

  /**
   * This method is called whenever the routing.route_dynamic event is
   * dispatched.
   *
   * @param GetResponseEvent $event
   */
  public function onRegister($event) {
    if ($event->getProvider() != 'polaris') {
      return;
    }
    $user = $event->getAccount();
    $data = $event->getData();

    // Give user customer role.
    $this->ensureRole($user);

    // Get user's customer profile.
    $profile = $this->getProfile($user);

    // Save their preferred branch location.
    if (is_object($data) && ($location = $this->getLocation($data))) {
      $profile->field_preferred_location->setValue($location);
    }

    $profile->field_first_name->setValue($data->NameFirst);
    $profile->field_middle_name->setValue($data->NameMiddle);
    $profile->field_last_name->setValue($data->NameLast);
    $profile->field_legal_first_name->setValue($data->LegalNameFirst);
    $profile->field_legal_middle_name->setValue($data->LegalNameMiddle);
    $profile->field_legal_last_name->setValue($data->LegalNameLast);
    if (!empty($data->PhoneNumber)) {
      $profile->field_phone->setValue($data->PhoneNumber);
    }
    if (!empty($data->EmailAddress)) {
      $profile->field_email_address->setValue($data->EmailAddress);
    }
    if ($profile->hasField('field_barcode')) {
      $profile->field_barcode->setValue($data->Barcode);
    }
    $profile->save();
  }

  /**
   *
   */
  private function getProfile(UserInterface $user) {
    $profile = $this->entityTypeManager->getStorage('profile')->loadByUser($user, 'customer');
    if (empty($profile)) {
      $profile = $this->entityTypeManager->getStorage('profile')->create([
        'type' => 'customer',
        'uid' => $user->id(),
      ]);
    }
    return $profile;
  }

  /**
   *
   */
  private function getLocation(\StdClass $data) {
    $query = $this->entityTypeManager->getStorage('node')->getQuery()
      ->accessCheck(TRUE)
      ->condition('type', 'location')
      ->condition('field_polaris_id', $data->PatronOrgID, '=')
      ->execute();
    return !empty($query) ? reset($query) : FALSE;
  }

  /**
   *
   */
  private function ensureRole(UserInterface $user) {
    if (!$role = Role::load('intercept_registered_customer')) {
      return FALSE;
    }
    if (!$user->hasRole('intercept_registered_customer')) {
      $user->addRole('intercept_registered_customer');
      $user->save();
    }
  }

}
