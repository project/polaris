<?php
/**
 * @file
 * Contains \Drupal\polaris\Form\ChangePinForm.
 */
namespace Drupal\polaris\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ChangePinForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'change_pin_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['header'] = [
      '#markup' => '<h1>Change Your PIN</h1>',
    ];
    // Get the library card number for the current user.
    $library_card_number = polaris_get_library_card_number();
    if (!empty($library_card_number)) {
      $form['intro'] = [
        '#markup' => '<p>Please enter your new PIN below and we\'ll change it for you! It\'s as simple as that.</p>',
      ];
      // @TODO: Change to 4-character maximum, PASSWORD fields.
      $form['new_pin'] = [
        '#type' => 'password',
        '#title' => t('New PIN:'),
        '#size' => 4,
        '#required' => TRUE,
      ];
      $form['new_pin_confirm'] = [
        '#type' => 'password',
        '#title' => t('Re-enter New PIN:'),
        '#size' => 4,
        '#required' => TRUE,
      ];
      $form['intro'] = [
        '#markup' => '<p>PIN numbers must be exactly four characters and can only contain numbers.</p>',
      ];
      $form['actions']['#type'] = 'actions';
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => $this->t('Change PIN'),
        '#button_type' => 'primary',
      ];
    }
    else {
      $this->messenger()->addError('It doesn\'t appear that you\'re logged in with a valid customer account with a library card number. You might need to try logging out and logging back in again.');
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    $new_pin = $form_state->getValue('new_pin');
    $new_pin_confirm = $form_state->getValue('new_pin_confirm');
    if ($new_pin != $new_pin_confirm) {
      $form_state->setErrorByName('new_pin', $this->t('The entered PIN numbers do not match.'));
    }
    if (!is_numeric($new_pin)) {
      $form_state->setErrorByName('new_pin', $this->t('Your PIN can only contain numbers.'));
    }
    if (strlen($new_pin) != 4) {
      $form_state->setErrorByName('new_pin', $this->t('Your PIN must be exactly four characters long.'));
    }

    // DEBUGGING
    //$form_state->setErrorByName('library_card_number', $this->t('Debugging.'));
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Get the library card number for the current user.
    $library_card_number = polaris_get_library_card_number();
    if (!empty($library_card_number)) {
      // Update the pin number for that library card number.
      $client = \Drupal::service('polaris.client');
      $patron = $client->patron->get($library_card_number);
      $patron->Password = $form_state->getValue('new_pin');
      $patron->update();

      $this->messenger()->addStatus(t('Your PIN was updated.'));
      $form_state->setRedirect('user.page');
    }
    else {
      $this->messenger()->addError(t('An error occurred and your PIN could not be updated.'));
    }
  }

}
