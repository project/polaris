<?php
/**
 * @file
 * Contains \Drupal\polaris\Form\ForgotPinForm.
 */
namespace Drupal\polaris\Form;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

class ForgotPinForm extends FormBase {
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'forgot_pin_form';
  }
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['header'] = [
      '#markup' => '<h1>Forgot Your PIN?</h1>',
    ];
    $form['intro'] = [
      '#markup' => '<p>If you know your library card number please enter it below. We\'ll send an email to you with replacement login info.</p>',
    ];
    $form['library_card_number'] = [
      '#type' => 'textfield',
      '#title' => t('Library Card Number:'),
      '#required' => TRUE,
    ];
    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Email Me'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    // Check with Polaris to see if we can validate that the submitted library
    // card number is in fact a valid account.
    $library_card_number = trim($form_state->getValue('library_card_number'));
    if (empty($library_card_number)) {
      $form_state->setErrorByName('library_card_number', $this->t('You must enter a library card number.'));
    }
    // Note: Since the PatronSearch method doesn't allow us to search by
    // username, we only allow this form to be submitted with library card #.
    // Other methods such as PatronValidate require the PIN number which we
    // don't have in this context.
    $client = \Drupal::service('polaris.client');
    $patron = $client->patron->get($library_card_number);
    $data = $patron->basicData();
    // Check expiration date.
    $circulation_blocks = $patron->circulateBlocksGet();
    preg_match('/\/Date\((-*)(\d+)(-\d+)\)\//', $circulation_blocks->ExpirationDate, $date);
    $expiration_date = $date[1] . $date[2]/1000;
    if ($data && $expiration_date < time()) { // This card is expired.
      // Trigger ExpiredAccountSubscriber to log them out and display a message.
      $response = new RedirectResponse('/polaris/forgot-pin-submitted?exp=2');
      $response->send();
    }
    else { // Card is not expired.
      // If it's not valid, let the customer know.
      if (empty($data->PatronID)) {
        $form_state->setErrorByName('library_card_number', $this->t('Sorry, we weren\'t able to find you in our records. Please double-check and try re-entering your information.'));
      }
      else {
        // Pass the discovered email address to the submit handler.
        $email_address = $data->EmailAddress;
        $form_state->setValue('email_address', $email_address);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $library_card_number = $form_state->getValue('library_card_number');
    $email_address = $form_state->getValue('email_address');

    // Mail one time login URL and instructions using current language.
    $mailManager = \Drupal::service('plugin.manager.mail');
    $module = 'polaris';
    $key = 'forgot-pin';
    $to = $email_address;
    $langcode = \Drupal::languageManager()->getCurrentLanguage()->getId();
    $params = [
      'library_card_number' => $library_card_number,
      'email_address' => $email_address,
    ];
    $send = true;
    $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    if ($result['result'] !== true) {
      $this->messenger()->addError(t('There was a problem sending your message and it was not sent.'));
    }
    else {
      $this->messenger()->addStatus($this->t('Further instructions have been sent to your email address.'));
    }

    // Redirect to the front page.
    $form_state->setRedirect('<front>');
  }

}
